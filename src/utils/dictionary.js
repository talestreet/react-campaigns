const language = window.sessionStorage.getItem("lng");
const data = {
  "en-in": {
    tabs_upcoming: "Upcoming",
    tabs_live: "Live",
    tabs_past: "Past",
    manage_campaigns: "Manage Campaigns",
    pricing: "Pricing",
    date: "Date",
    campaign: "Campaign",
    actions: "Actions",
  },
  de: {
    tabs_upcoming: "Bevorstehende",
    tabs_live: "Leben",
    tabs_past: "Vergangenheit",
    manage_campaigns: "Kampagnen verwalten",
    pricing: "Preisgestaltung",
    date: "Aktionen",
    campaign: "Kampagne",
    actions: "Aktionen",
  },
};

const t = (key) => {
  return data[language] ? data[language][key] : data["en-in"][key];
};

export { t };
