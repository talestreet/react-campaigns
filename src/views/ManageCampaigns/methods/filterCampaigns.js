import moment from "moment";
/*
It considers:
Live campaigns: Which have a time for today's day in user's machine timzone
. subsequently: All upcoming is for next day or later ones,
*/
const filterCampaigns = (response) => {
  return response.reduce(
    (acc, campaign) => {
      const { createdOn } = campaign;
      const days = moment()
        .startOf("day")
        .diff(moment(createdOn).startOf("day"), "days");
      if (days > 0) {
        acc.past.push(campaign);
      } else if (days < 0) {
        acc.upcoming.push(campaign);
      } else {
        acc.live.push(campaign);
      }
      return acc;
    },
    { upcoming: [], past: [], live: [] }
  );
};

export default filterCampaigns;
