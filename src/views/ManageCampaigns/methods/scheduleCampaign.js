import moment from "moment";
/*
It considers:
Live campaigns: Which have a time for today's day in user's machine timzone
. subsequently: All upcoming is for next day or later ones,
*/
const scheduleCampaign = ({ id: campaignId, date, campaigns, activeTab }) => {
  const newDate = moment(date).unix() * 1000;

  //**---Find and extract**---//
  const activeTabCampaigns = [...campaigns[activeTab]];
  const index = activeTabCampaigns.findIndex(({ id }) => id === campaignId);
  const campaign = activeTabCampaigns.splice(index, 1)[0]; //Find and remove the campaign by detecting current active list
  //**---Find and extract**---//

  campaign.createdOn = newDate;
  campaigns[activeTab] = activeTabCampaigns;

  const days = moment()
    .startOf("day")
    .diff(moment(date).startOf("day"), "days");
  const newTab = findNewTab(days);
  campaigns[newTab].unshift(campaign);
  return campaigns;
};

const findNewTab = (days) => {
  if (days > 0) return "past";
  if (days < 0) return "upcoming";
  return "live";
};

export default scheduleCampaign;
