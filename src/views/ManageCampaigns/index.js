import React, { useState, useEffect } from "react";
import { SwitchTabs } from "components/";
import style from "./_manageCampaigns.module.scss";
import { filterCampaigns, scheduleCampaign } from "./methods";
import Table from "./components/Table";
import { t } from "utils/dictionary";
const ManageCampaigns = () => {
  const [activeTab, switchTab] = useState("upcoming");
  const [campaigns, setCampaigns] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = async () => {
    try {
      const res = await fetch(
        "https://api.jsonbin.io/b/5f36f7ffaf209d1016bb9a1a/3",
        {
          headers: {
            "secret-key":
              "$2b$10$TLdHFuuZ9Chu8HHpjrrVLeF6JyAe..ovnNFaYgGKcEptjVch9SZrS",
          },
        }
      );
      const response = await res.json();
      setCampaigns(filterCampaigns(response));
      setLoading(false);
    } catch (e) {
      setError("Error");
      setLoading(false);
    }
  };

  const scheduleAgain = (obj) => {
    const updated = scheduleCampaign({
      campaigns: { ...campaigns },
      ...obj,
    });
    setCampaigns(updated);
  };
  return (
    <div className={style.wrapper}>
      <h2 className={style.heading}>{t("manage_campaigns")}</h2>
      <SwitchTabs toggle={switchTab} activeTab={activeTab} />
      <Table
        loading={loading}
        error={error}
        data={campaigns[activeTab] || []}
        scheduleAgain={scheduleAgain}
        activeTab={activeTab}
      />
    </div>
  );
};

export default ManageCampaigns;
