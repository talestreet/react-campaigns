import React from "react";
import Entries from "./Entries";
import Headings from "./Headings";
import style from "./_table.module.scss";
const Table = ({ data, scheduleAgain, loading, error, activeTab }) => {
  return (
    <div className={style.table}>
      <Headings />
      {loading && <div className="loader">loading...</div>}
      {error && "Error: Something went wrong"}
      {!loading && !error && (
        <Entries
          data={data}
          scheduleAgain={scheduleAgain}
          activeTab={activeTab}
        />
      )}
    </div>
  );
};
export default Table;
