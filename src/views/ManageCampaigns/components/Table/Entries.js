import React, { useState } from "react";
import moment from "moment";
import { Row, Col, UncontrolledPopover, PopoverBody } from "reactstrap";
import csvIcon from "assets/icons/file.png";
import priceIcon from "assets/icons/price.png";
import calendarIcon from "assets/icons/calendar.png";
import statsIcon from "assets/icons/stats.png";
import style from "./_table.module.scss";
import cIcon from "assets/bitmap.png";
import DatePicker from "react-datepicker";
import PricingModal from "components/PricingModal";
import "react-datepicker/dist/react-datepicker.css";
const Entries = ({ data, scheduleAgain, activeTab }) => {
  const [pricingModal, setPricingModal] = useState(null);
  const togglePricingModal = () => {
    setPricingModal(null);
  };
  if (data.length < 1) {
    return (
      <Row className={`${style.row} no-gutters`}>
        <Col>Nothing to show</Col>
      </Row>
    );
  }
  return (
    <>
      {data.map(({ name, createdOn, id }) => {
        return (
          <Row className={`${style.row} no-gutters`} key={id}>
            <Col
              lg={{ size: 3, order: 2 }}
              className={style.campaignDetailsColumn}
            >
              <img
                src={cIcon}
                height="40px"
                className={style.campaignImage}
                alt="icon"
              />

              <div className={style.campaignDetails}>
                <div className={style.campaignName}>{name}</div>
                <div className={style.campaignCountry}>US</div>
              </div>
            </Col>
            <Col lg={{ size: 2, order: 1 }} className={style.dateColumn}>
              <div className={style.date}>
                {" "}
                {moment(createdOn).format("MMM Do YY")}
              </div>
              {activeTab !== "live" && (
                <div className={style.moment}>
                  {moment(
                    moment(createdOn).format("MMM Do YY"),
                    "MMM Do YY"
                  ).fromNow()}
                </div>
              )}
            </Col>
            <Col lg={{ size: 2, order: 3 }} className={style.priceColumn}>
              <span
                className={style.action}
                onClick={() => {
                  setPricingModal(id);
                }}
              >
                <img src={priceIcon} height="24px" alt="pricing" /> View Pricing
              </span>
            </Col>
            <Col lg={{ size: 5, order: 4 }} className={style.actionColumn}>
              <span className={style.action}>
                <img src={csvIcon} height="24px" alt="csv" /> CSV
              </span>
              <span className={style.action}>
                <img src={statsIcon} height="24px" alt="report" />
                Report
              </span>
              <span className={style.action} id={`schedule_${id}`}>
                <img src={calendarIcon} height="24px" alt="schedule" />
                Schedule Again
              </span>

              <UncontrolledPopover
                placement="bottom"
                trigger="legacy"
                target={`schedule_${id}`}
              >
                <PopoverBody className="p-0">
                  <DatePicker
                    onChange={(date) => scheduleAgain({ id, date, activeTab })}
                    inline
                  />
                </PopoverBody>
              </UncontrolledPopover>
            </Col>
          </Row>
        );
      })}
      <PricingModal isOpen={pricingModal} toggle={togglePricingModal} />
    </>
  );
};
export default Entries;
