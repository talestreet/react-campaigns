import React from "react";
import { Row, Col } from "reactstrap";
import style from "./_table.module.scss";
import { t } from "utils/dictionary";

var classnames = require("classnames");
const TableHeadings = () => {
  return (
    <Row
      className={classnames({
        [style.headings]: true,
        "no-gutters": true,
      })}
    >
      <Col lg="2">{t("date")}</Col>
      <Col lg="3">{t("campaign")}</Col>
      <Col lg="2">{t("pricing")}</Col>
      <Col lg="5">{t("actions")}</Col>
    </Row>
  );
};
export default TableHeadings;
