import React from "react";
import logo from "assets/logo.png";
import style from "./_header.module.scss";
import { LanguageSelector } from "components";
const Header = (props) => {
  return (
    <div className={style.header}>
      <img src={logo} height="40px" alt="logo" />
      <LanguageSelector className={style.languageSelector} />
    </div>
  );
};

export default Header;
