import React, { useState } from "react";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import style from "./_languageSelector.module.scss";
var classnames = require("classnames");
const LanguageSelector = ({ className }) => {
  const [dropdownOpen, setOpen] = useState(false);
  const toggle = () => setOpen(!dropdownOpen);
  const language = window.sessionStorage.getItem("lng") || "en-in";

  const changeLanguage = (lng) => {
    if (language === lng) return;
    window.sessionStorage.setItem("lng", lng);
    window.location.reload();
  };
  return (
    <ButtonDropdown
      isOpen={dropdownOpen}
      toggle={toggle}
      className={`${style.wrapper} ${className}`}
    >
      <DropdownToggle caret>Select Language</DropdownToggle>
      <DropdownMenu>
        <DropdownItem
          className={classnames({
            [style.language]: true,
            [style.selected]: language === "en-in",
          })}
          onClick={() => changeLanguage("en-in")}
        >
          English
        </DropdownItem>
        <DropdownItem
          className={classnames({
            [style.language]: true,
            [style.selected]: language === "de",
          })}
          onClick={() => changeLanguage("de")}
        >
          German
        </DropdownItem>
      </DropdownMenu>
    </ButtonDropdown>
  );
};

export default LanguageSelector;
