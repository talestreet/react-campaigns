import React from "react";
import { Modal, ModalBody, Row, Col } from "reactstrap";
import cIcon from "assets/popup.png";
import style from "./_pricingModal.module.scss";
const PricingModal = ({ isOpen, toggle }) => {
  return (
    <Modal isOpen={isOpen} toggle={toggle} className={style.modal}>
      <ModalBody>
        <div className={style.header}>
          <div className={style.image}>
            <img src={cIcon} width="137px" alt="icon" />
          </div>
          <div className={style.detailColumn}>
            <div className={style.details}>
              <h6>PubG</h6>
              <span>US</span>
            </div>
          </div>
        </div>
        <div className={style.pricingWrapper}>
          <h6>Pricing</h6>
          <PricingRows pricingData={pricingData} />
        </div>
        <button className={style.button} onClick={toggle}>
          Close
        </button>
      </ModalBody>
    </Modal>
  );
};

const PricingRows = ({ pricingData }) => {
  return pricingData.map(({ range, currency, price }) => {
    return (
      <Row className={style.pricingRow}>
        <Col className={style.timePeriod}>{range}</Col>
        <Col className={style.price}>
          {currency} {price}
        </Col>
      </Row>
    );
  });
};

const pricingData = [
  {
    range: "1 month",
    price: "25",
    currency: "$",
  },
  {
    range: "1 month",
    price: "25",
    currency: "$",
  },
  {
    range: "1 month",
    price: "25",
    currency: "$",
  },
];
export default PricingModal;
