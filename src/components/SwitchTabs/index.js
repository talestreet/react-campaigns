import React from "react";
import style from "./_switchTabs.module.scss";
import { t } from "utils/dictionary";

var classnames = require("classnames");
const SwitchTabs = ({ activeTab, toggle }) => {
  return (
    <div className={`clearfix ${style.tabWrapper}`}>
      <div
        className={classnames({
          [style.active]: activeTab === "upcoming",
          [style.tab]: "true",
        })}
        onClick={() => {
          toggle("upcoming");
        }}
      >
        {t("tabs_upcoming")}
      </div>
      <div
        className={classnames({
          [style.active]: activeTab === "live",
          [style.tab]: "true",
        })}
        onClick={() => {
          toggle("live");
        }}
      >
        {t("tabs_live")}
      </div>
      <div
        className={classnames({
          [style.active]: activeTab === "past",
          [style.tab]: "true",
        })}
        onClick={() => {
          toggle("past");
        }}
      >
        {t("tabs_past")}
      </div>
    </div>
  );
};
export default SwitchTabs;
