import React, { useEffect } from "react";
import { Container } from "reactstrap";
import Layout from "./Layout";
import ManageCampaigns from "views/ManageCampaigns";
import moment from "moment";
import "moment/locale/de";
import "moment/locale/en-in";
function App() {
  useEffect(() => {
    const lng = window.sessionStorage.getItem("lng");
    if (lng) {
      moment.locale(lng);
    }
  }, []);
  return (
    <Layout>
      <Container>
        <ManageCampaigns />
      </Container>
    </Layout>
  );
}

export default App;

// export { DetailsProvider, DetailsConsumer, DetailsContext };
